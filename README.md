# The Weekend Recreate (TWR) | Semantic & Forms

Recreating and enhancing code of my old project that aims to deepen personal knowledge of HTML & CSS.

## Description

Recreating glintsXprogate week 2 project ("The Glints x Progate" webpage) by enhancing the code to be semantically correct for better readability and adding new feature in form section. 

## Authors

raihan(period.)rachman

## License

This project is licensed under the [Raihan Rachman] License - see the LICENSE.md file for details

## Acknowledgments

* updatedlater..